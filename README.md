![2048](/Source/SystemAssets/card.png)

# 2048 for the Playdate

## Controls

- `d-Pad` or `Crank`: slide tiles in that direction
- `A` (long press): start a new game

## Source Code

You can compile the code with the following command:

```sh
Build and Run (Simulator).ps1
```

Script will do all work!
